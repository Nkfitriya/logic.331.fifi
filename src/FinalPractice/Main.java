package FinalPractice;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("Pilih Soal (1 - 10)");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 10) {
                System.out.println("Pilihan Tidak Tersedia");
                pilihan = input.nextInt();
            }


            switch (pilihan) {
                case 1:
                    EsLoli01.Resolve();
                    break;
                case 2:
                    PalindromeYorN02.Resolve();
                    break;
                case 3:
                    RotasiDeret03.Resolve();
                    break;
                case 4:
                    break;
                case 5:
                    jam05.Resolve();
                    break;
                case 6:
                    Perpustakaanl06.resolve();
                    break;
                case 7:
                    deret07.Resolve();
                    break;
                case 8:
                    break;
                case 9:
                    DerajatJam09.resolve();
                    break;
                case 10:
                    Cupcake10.Resolve();
                    break;
                default:
            }
            System.out.println("Try Again? y/n");
            input.nextLine();
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")){
                flag = false;
            }
        }
    }
}