package FinalPractice;

import java.util.Arrays;
import java.util.Scanner;

public class PalindromeYorN02 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kata/angka = ");
        String masukan = input.nextLine();

        char[] masukanchar = masukan.toCharArray();
        char[] masukaancharbalik = new char[masukanchar.length];

        int count = 1;
        for (int i = 0; i < masukanchar.length; i++) {
            masukaancharbalik[i] = masukanchar[masukanchar.length-count];
            count++;
        }
        int  helper = 0;
        for (int i = 0; i < masukanchar.length; i++) {
            if (masukanchar[i] == masukaancharbalik[i]){
                helper++;
            }
        }
        if (helper == masukanchar.length){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
    }
}