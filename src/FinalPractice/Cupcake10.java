package FinalPractice;

import java.util.Scanner;

public class Cupcake10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan jumlah kue yang ingin dibuat(n) = ");
        int n = input.nextInt();

        double terigu = (double) 125/15;
        double gula = (double) 100/15;
        double susu = (double) 100/15;

        int helper1 = (int) (n * terigu);
        int helper2 = (int) (n * gula);
        int helper3 = (int) (n * susu);

        System.out.println("jumlah terigu yang butuhkan untuk " + n + " Cupcake. adalah " + helper1 + " gr");
        System.out.println("jumlah gula yang butuhkan untuk " + n + " Cupcake. adalah " + helper2 + " gr");
        System.out.println("jumlah susu yang butuhkan untuk " + n + " Cupcake. adalah " + helper3 + " ml");
    }
}