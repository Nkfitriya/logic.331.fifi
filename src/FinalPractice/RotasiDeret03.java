package FinalPractice;

import java.util.Scanner;

public class RotasiDeret03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input deret angka : ");
        String deretAngka = input.nextLine();
        System.out.println("Masukan berapa kali rotasi : ");
        int n = input.nextInt();
        String [] textArray = deretAngka.split(" ");


        for (int i = 0; i < n; i++) {
            String temp = textArray[0];
            for (int j = 0; j < textArray.length -1; j++) {
                textArray[j] = textArray[j + 1];
            }
            textArray[textArray.length - 1] = temp;
            System.out.println("Cetak hasil rotasi : ");
            for (int k = 0; k < textArray.length; k++) {
                System.out.print(textArray[k] + " ");
            }
            System.out.println();
        }

    }
}