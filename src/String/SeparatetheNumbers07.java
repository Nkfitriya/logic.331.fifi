package String;

import java.util.Scanner;

public class SeparatetheNumbers07 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Separate +========");
        System.out.println("Contoh Input = 91011 100101102 9991000");
        System.out.println("Masukkan input = ");
        String answer = input.nextLine();
        String[] allAnswer = answer.split(" ");
        isBeautiful(allAnswer);
    }

    private static void isBeautiful(String[] dataArray) {
        System.out.println("Output = ");
        for (int i = 0; i < dataArray.length; i++) {
            String dataInput = dataArray[i];
            String subString = "";
            // Untuk menandai beautiful atau tidak
            boolean isValid = false;

            // Mengulangi sebanyak 1/2 panjang dari data
            for (int j = 1; j <= (dataInput.length() / 2); j++) {
                // Ambil Nilai Awalnya
                subString = dataInput.substring(0, j);
                // Kemudian dijadikan Conversi dari string ke integer / Long
                int num = Integer.parseInt(subString);
                String validString = subString;
                // Ketika panjang Data awal kurang dari panjang data input
                // Ambil data awal, kemudian ditambahkan nilainya didepannya
                while (validString.length() < dataInput.length()) {
                    // Menggunakan post incement
                    validString += Integer.toString(++num);
                }
                // Memastikan bahwa nilai Input sama dengan hasil String Validasi
                if (dataInput.equals(validString)) {
                    isValid = true;
                    break;
                }
            }

            if (isValid) {
                System.out.println("YES " + subString);
            } else {
                System.out.println("NO");
            }
        }
        Utility.mauLagi();
    }
}
