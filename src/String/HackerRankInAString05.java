package String;

import java.util.Scanner;

public class HackerRankInAString05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat HackerRank = ");
        String kalimat = input.nextLine();
        char[] kalimatArray = kalimat.toLowerCase().toCharArray();

        String kalimatHacker = "hackerrank";
        char[] hackerArray = kalimatHacker.toCharArray();
        int helper = 0;


        for (int i = 0; i < kalimatArray.length; i++) {
            if (kalimatArray[i] == hackerArray[helper]){
                helper++;
            }
        }
        if (helper == hackerArray.length) {
            System.out.println("true");
        }
        else if (helper != hackerArray.length){
            System.out.println("false");
        }
    }
}

