package String;

import java.util.Scanner;

public class StrongPassword02 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("password yang harus ada = ");
        System.out.println("abcdefghijklmnopqrstuvwxyz");
        System.out.println("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        System.out.println("!@#$%^&*()-+");
        System.out.println(1234567890);
        System.out.println("harus terdiri dari minimal 6 digit");
        System.out.println();

        String lowerCase = "abcdefghijklmnopqrstuvwxyz";
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String caracter = "!@#$%^&*()-+";
        String angka = "1234567890";

        boolean flag = true;

        while (flag){
            System.out.print("masukan password = ");
            String password = input.nextLine();

            for (int i = 0; i < password.length(); i++) {
                if (password.length() <= 6){
                    System.out.println("password harus minimal 6 digit");
                    flag = true;
                }
                if (!password.contains(lowerCase.substring(i, i+1))) {
                    System.out.println("minimal ada huruf kecil");
                    flag = true;
                }
                else if (!password.contains(upperCase.substring(i, i+1))) {
                    System.out.println("minimal ada huruf besar");
                    flag = true;
                }
                else if (!password.contains(caracter.substring(i, i+1))){
                    System.out.println("kurang karakter");
                    flag = true;
                }
                else if (!password.contains(angka.substring(i,i+1))){
                    System.out.println("kurang angka");
                    flag = true;
                }
                else {
                    System.out.println("tidak ada yang kurang");
                    flag = false;
                }
            }
        }
    }
}
