package String;

import java.util.Scanner;

public class MarsExploration04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat SOS = ");
        String kalimat = input.nextLine();
        int result = 0;
        char[] kalimatSos = kalimat.toLowerCase().toCharArray();

        for (int i = 0; i < kalimatSos.length; i+=3) {
            if(kalimatSos[i] == 's' && kalimatSos[i + 1] == 'o' && kalimatSos[i + 2] == 's'){
                result += 1;
            }
        }
        System.out.println(result);
    }
}

