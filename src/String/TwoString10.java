package String;

import java.util.Scanner;

public class TwoString10 {
    public static void Resolve(){
        Scanner scanner = new Scanner(System.in);
        String string1, string2;
        int count = 0;

        System.out.print("String 1: ");
        string1 = scanner.nextLine();

        System.out.print("String 2: ");
        string2 = scanner.nextLine();

        char[] strings1 = string1.toCharArray();
        char[] strings2 = string2.toCharArray();

        for (int i = 0; i < strings1.length; i++) {
            for (int j = 0; j < strings2.length; j++) {
                if (strings1[i] == strings2[j]){
                    count++;
                }
            }
        }

        if (count != 0){
            System.out.println("YES");
        }
        else {
            System.out.println("NO");
        }
    }
}