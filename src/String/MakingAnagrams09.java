package String;

import java.util.HashMap;
import java.util.Scanner;

public class MakingAnagrams09 {
    public static void Resolve(){
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        char keyword = 'c';

        System.out.print("Input 1: ");
        String s1 = scanner.nextLine();

        System.out.print("Input 2: ");
        String s2 = scanner.nextLine();

        char[] charsS1 = s1.toCharArray();
        char[] charsS2 = s2.toCharArray();

        for (int i = 0; i < 2; i++) {
            if (i == 0){
                for (int j = 0; j < charsS1.length; j++) {
                    if (charsS1[j] != 'c'){
                        count++;
                    }
                }
            }
            else if (i == 1) {
                for (int j = 0; j < charsS2.length; j++) {
                    if (charsS2[j] != 'c'){
                        count++;
                    }
                }
            }
        }

        System.out.println(count);
    }
}

