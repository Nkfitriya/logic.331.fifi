package String;

import java.util.Scanner;

public class Pangrams06 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan kalimat yang mengandung pangrams");
        String kalimat = input.nextLine();

        String alfabet = "abcdefghijklmnopqrstuvwxyz";

        int helper = 0;

        for (int i = 0; i < alfabet.length(); i++) {
            if (kalimat.contains(alfabet.substring(i, i+1))){
                helper++;
            }
        }
        if (helper == 26){
            System.out.println("pangrams");
        } else {
            System.out.println("no pangrams");
        }
    }
}

