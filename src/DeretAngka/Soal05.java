package DeretAngka;

// Soal 04
// n = 7
// 1	5	9	13	17	21	25

public class Soal05 {
    public static void Resolve(int n) {
        int helper = 1;
        int[] results = new int[n];
        int count = 1;

        for (int i = 0; i < n; i++) {
            if (count % 3 == 0) {
                results[i] =0 ;
                count++;
            } else {
                results[i] = helper;
                helper += 4;
                count++;
            }
        }
        Utility.PrintArray1D(results);
    }

}
