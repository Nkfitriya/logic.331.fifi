package DeretAngka;


// Soal 08
// n = 7
// 3	9	27	81	243	729	2187

public class Soal08 {
    public static void Resolve(int n){
        int helper = 3;
        int[] results =new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper *=3;

        }
        Utility.PrintArray1D(results);
    }
}
