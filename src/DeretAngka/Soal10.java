package DeretAngka;

// Soal 09
// n = 7
// 4	16	*	64	256	*	1024

public class Soal10 {
    public static void Resolve(int n){
        int helper = 3;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper *= 3;
        }
        Utility.PrintXXX(results);

    }
}
