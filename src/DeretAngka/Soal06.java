package DeretAngka;

// Soal 06
// n = 7
// 1	5	*	13	17	*	25

public class Soal06 {
    public static void Resolved(int n) {
        int helper1 = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = helper1;
            helper1 += 4;
        }
        Utility.Bintang(hasil);
    }
}