package DeretAngka;

// Soal 04
// n = 7
// 1	5	9	13	17	21	25

public class Soal04 {
    public static void Resolve(int n){
        int helper = 1;
        int[] results =new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper +=4;

        }
        Utility.PrintArray1D(results);
    }
}
