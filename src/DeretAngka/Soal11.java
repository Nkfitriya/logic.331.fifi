package DeretAngka;

// Soal 11 fibonaci
// n = 7
// 1	1	2	3	5	8	13

public class Soal11 {
    public static void Resolve(int n) {
        int helper = 1;
        int awal = 0;
        int sesudah = 1;
        int[] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper = awal + sesudah;
            awal = sesudah;
            sesudah = helper;

        }
        Utility.PrintArray1D(results);
    }
}
