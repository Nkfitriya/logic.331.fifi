package DeretAngka;

// Soal 07
// n = 7
// 2	4	8	16	32	64	128

public class Soal07 {
    public static void Resolve(int n){
        int helper = 2;
        int[] results =new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper *=2;

        }
        Utility.PrintArray1D(results);
    }
}
