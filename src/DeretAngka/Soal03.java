package DeretAngka;

// Soal 03
// n = 7
// 1	4	7	10	13	16	19

public class Soal03 {
    public static void Resolve(int n){
        int helper = 1;
        int[] results =new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper +=3;

        }
        Utility.PrintArray1D(results);
    }
}
