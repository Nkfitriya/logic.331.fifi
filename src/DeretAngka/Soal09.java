package DeretAngka;

// Soal 09
// n = 7
// 4	16	*	64	256	*	1024

public class Soal09 {
    public static void Resolve(int n) {
        int helper = 4;
        int[] results = new int[n];
        int count = 1;

        for (int i = 0; i < n; i++) {
            if (count % 3 == 0) {
                results[i] =0 ;
                count++;
            } else {
                results[i] = helper;
                helper *= 4;
                count++;
            }

        }
        Utility.PrintArray1D(results);
    }
}
