package DeretAngka;

// Soal 02
// n = 7
// 2	4	6	8	10	12	14


public class Soal02 {
    public static void Resolve(int n){
        int helper = 2;
        int[] results =new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = helper;
            helper +=2;

        }
        Utility.PrintArray1D(results);
    }
}
