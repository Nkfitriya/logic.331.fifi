package DeretAngka;

public class Utility {
    public static void PrintArray1D(int[] results)
    {
        for (int i = 0; i < results.length; i++) {
            if (results[i] == 0){
                System.out.print("*  ");
            } else {
                System.out.print(results[i] + " ");
            }
        }
        System.out.println();
    }
    public static void Bintang(int[] hasil) {
        for (int i = 0; i < hasil.length; i++) {
            if (hasil[i] % 3 == 0) {
                System.out.print("* ");
            } else {
                System.out.print(hasil[i] + " ");
            }
        }
        System.out.println("");
    }
    public static void Pagar(int[][] hasil)
    {
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil[0].length; j++) {
                if (hasil[i][j] == 0) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println(" ");
            System.out.println(" ");
        }
    }
    public static void PrintXXX(int[] results){
        int count = 0;
        for (int i = 0; i < results.length; i++) {
            if (count == 3){
                System.out.print("xxx ");
                count -= 4;
            }
            else {
                System.out.print(results[i] + " ");
                count++;
            }
        }
        System.out.println();
    }
}
