package InputPlural;

import java.util.Scanner;

//        Soal 06
//        Hitung pasangan bilangan pada deret berikut
//        Contoh:
//
//        Input: 2	1	2	3	1	3	4
//        Output: 3

public class Soal06 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk menghitung pasangan bilangan pada deret dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi");
        String text = input.nextLine();

        int[] nilai = Utility.ConvertStringToArrayInt(text);
        int panjang = nilai.length;
        int counter = 0;
        int[] banyaknya = new int[panjang];
        boolean isSame = false;

        for (int i = 0; i < panjang; i++) {
            if (i == 0) {
                for (int j = 0; j < panjang; j++) {
                    if (nilai[i] == nilai[j]) {
                        counter++;
                    }
                }
            } else {
                isSame = false;
                for (int j = 0; j < i; j++) {
                    if (nilai[i] == nilai[j]) {
                        isSame = true;
                    }
                }
                if (isSame) {
                    counter = 0;
                } else {
                    for (int j = 0; j < panjang; j++) {
                        if (nilai[i] == nilai[j]) {
                            counter++;
                        }
                    }
                }
            }
            banyaknya[i] = counter;
            counter = 0;
        }
        System.out.println();
        int output = 0;
        for (int i = 0; i < panjang; i++) {
            output += banyaknya[i] / 2;
        }

        System.out.println(output);

    }
}
