package InputPlural;

import java.text.DecimalFormat;
import java.util.Scanner;

//Soal 09
//        Tentukan banyaknya persentase angka yang postif, negatif, dan 0
//        Contoh:
//
//        Input: -4	2	-5	0	1	10
//        Output: Positive = 50 %
//        Negative = 33.3 %
//        Zero	 = 16.6 %

public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk entukan banyaknya persentase angka yang positif, negatif, dan 0 pada deret dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi)");
        String text = input.nextLine();


        int[] intArray= Utility.ConvertStringToArrayInt(text);
        double lenght = intArray.length;
        double positif = 0;
        double negatif = 0;
        double zero = 0;

        for (int i = 0; i < lenght; i++) {
            if(intArray[i] > 0){
                positif++;
            } else if (intArray[i] < 0) {
                negatif++;
            } else if (intArray[i] == 0) {
                zero++;
            }
        }


        DecimalFormat desimal = new DecimalFormat("0.0");
        positif = ((positif/lenght) * 100);
        negatif = ((negatif/lenght) * 100);
        zero = ((zero/lenght) * 100);

        System.out.print("Positive = ");
        System.out.println(desimal.format(positif) + " %");
        System.out.print("Negative = ");
        System.out.println(desimal.format(negatif) + " %");
        System.out.print("Zero = ");
        System.out.println(desimal.format(zero) + " %");

    }
}