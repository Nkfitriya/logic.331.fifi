package InputPlural;

import java.util.Scanner;

//        Soal 10
//        Buatlah sebuah program yang bisa mencari tahu bilangan prima
//        Contoh:
//
//        Input:	2	4	5	7	43	10
//        Output: 2,5,7,43 adalah bilangan prima


public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk mentukan bilangan prima pada deret dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int lenght = intArray.length;
        int nilai = 0;
        int[] prima= new int[lenght];

        for (int i = 0; i < lenght; i++) {
            nilai = 0;
            for (int j = 0; j < intArray[i]; j++) {
                if (intArray[i] % (j+1) == 0){
                    nilai++;
                }
            }
            if (nilai == 2){
                prima[i]=nilai;
            }

        }
        for (int i = 0; i < lenght; i++) {
            if(prima[i] == 2){
                System.out.println(intArray[i] + " ");
            }
        }
    }
}