package InputPlural;

//        Soal 01
//        Jumlahkan semua angka di input
//        Contoh:
//
//        Input:	1	3	4	0	2
//        Output:	10


import java.util.Scanner;

public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);


        System.out.println("Program untuk Jumlahkan semua angka yang di input");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        int Sn = 0; // Jumlah nilai suku

        for (int i = 0; i < length; i++) {
            Sn = Sn+intArray[i];
        }
        System.out.println(Sn + " ");

    }
}

