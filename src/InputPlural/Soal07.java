package InputPlural;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

//        Soal 07
//        Tentukan jumlah terbesar dari n - 1 bilangan beserta jumlah terkecilnya
//        Contoh:
//
//        Input: 1	2	3	4	5
//        Outpu:  Terbesar: 14
//        Terkecil: 10


public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk jumlah terbesar dari n - 1 bilangan beserta jumlah terkecilnya pada deret dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        Arrays.sort(intArray);
        int nilaiMax = 0;
        int nilaiMin = 0;

        for (int i = 1; i < length; i++) {
            nilaiMax += intArray[i];
        }
        for (int i = 0; i < length - 1; i++) {
            nilaiMin += intArray[i];
        }
        System.out.println("nilai maksimal = " + nilaiMax);
        System.out.println("nilai minimal = " + nilaiMin);
    }

}
