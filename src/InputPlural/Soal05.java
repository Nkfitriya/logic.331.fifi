package InputPlural;

import java.util.Scanner;

//        Soal 05
//        Urutkanlah deret bilangan berikut tanpa menggunakan fungsi Sort
//        Contoh:
//
//        Input: 0	4	-5	3	10
//        Output: -5	0	3	4	10


public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk mengurutkanlah deret bilangan dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int hitung = 0;

        for (int i = 0; i < length; i++) {
            for (int j = i+1; j < length; j++) {
                if (intArray[i] > intArray[j])
                {
                    hitung = intArray[i];
                    intArray[i] = intArray[j];
                    intArray[j] = hitung;

                }
            }

        }
        for (int i = 0; i < length; i++) {
            System.out.print(intArray[i]+ " ");

        }
        System.out.println();
    }
}
