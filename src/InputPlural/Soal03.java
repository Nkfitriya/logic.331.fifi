package InputPlural;

import java.util.Scanner;

//        Soal 02
//        Cari rata-rata dari deret bilangan berikut
//        Contoh:
//
//        Input: 5	3	2	10	5
//        Output: 5

public class Soal03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk mencari nilai Median dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        double median = 0;
        if (length/2 == 0){
            median = intArray[((length+1)/2)-1];
            System.out.println(median);
        } else
            median = (double) (intArray[length/2] + intArray[(length/2)-1])/2;
        System.out.println(median);

    }
}
