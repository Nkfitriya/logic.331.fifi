package InputPlural;

import java.util.Scanner;


//        Soal 02
//        Cari rata-rata dari deret bilangan berikut
//        Contoh:
//
//        Input: 5	3	2	10	5
//        Output: 5

public class Soal02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Program untuk mencari nilai rata-rata dari angka yang diinputkan");
        System.out.println("Input Deret Angka (Pisah Angka Menggunakan Spasi)");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int jumlah = 0;

        for (int i = 0; i < length; i++) {
            jumlah = jumlah + intArray [i];
        }
        double results =  jumlah/length;
        System.out.println(results);
    }
}
