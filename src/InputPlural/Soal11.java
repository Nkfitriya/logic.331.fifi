package InputPlural;

import java.util.Scanner;

//        Soal 11
//        Urutkan alphabet dengan pilihan Ascending dan Descending
//        Contoh:
//
//        Input1: b	a	c	z	o
//        Input2: ASC
//
//        Output: a	b	c	o	z

public class Soal11 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan huruf : ");
        String input1 = input.nextLine();
        System.out.println("Pilih ASC atau DSC : ");
        String input2 = input.nextLine();

        char[] inputArray = input1.toLowerCase().toCharArray();
        int length = inputArray.length;
        char temp = 0;

        if (input2.equals("asc")) {
            for (int i = 0; i < length; i++) {
                for (int j = i+1; j < length; j++) {
                    if (inputArray[i] > inputArray[j]) {
                        temp = inputArray[i];
                        inputArray[i] = inputArray[j];
                        inputArray[j] = temp;
                    }
                }
            }

            for (int i = 0; i < length; i++) {
                System.out.print(inputArray[i] + " ");
            }
        } else if (input2.equals("dsc")) {
            for (int i = 0; i < length; i++) {
                for (int j = i+1; j < length; j++) {
                    if (inputArray[i] < inputArray[j]) {
                        temp = inputArray[i];
                        inputArray[i] = inputArray[j];
                        inputArray[j] = temp;
                    }
                }
            }

            for (int i = 0; i < length; i++) {
                System.out.print(inputArray[i] + " ");
            }
        }
        System.out.println();
    }
}