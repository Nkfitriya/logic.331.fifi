package InputPlural;

import java.util.Scanner;

//        Soal 08
//        Tentukan banyaknya angka terbesar pada deret berikut
//        Contoh:
//
//        Input: 2	3	4	1	5	5	2
//        Output: 2

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan deret angka = ");
        String text = input.nextLine();
        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int[] hasil = new int[length];

        int maxHeight = -1;
        int count = 0;

        for (int i = 0; i < length; i++) {
            int height = hasil[i]; // masih belum bisa
            if (height > maxHeight) {
                maxHeight = height;
                count = 1;
            } else if (height == maxHeight) {
                count++;
            }
        }
        System.out.println(count);
    }
}
