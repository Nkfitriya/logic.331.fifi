package Warmup;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("Choose The Question ( 1-10 )");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 10) {
                System.out.println("Option is Not Available");
                pilihan = input.nextInt();
            }

            switch (pilihan) {
                case 1:
                    Solvemefirst01.Resolve();
                    break;
                case 2:
                    break;
                case 3:
                    SimpleArraySum03.Resolve();
                    break;
                case 4:
                    DiagonalDifference04.Resolve();
                    break;
                case 5:
                    PlusMinus05.Resolve();
                    break;
                case 6:
                    Staircase06.Resolve();
                    break;
                case 7:
                    MinMaxSum07.Resolve();
                    break;
                case 8:
                    BirthdayCakeCandles08.Resolve();
                    break;
                case 9:
                    AVeryBigSum09.Resolve();
                    break;
                case 10:
                    ComparetheTripletsPage10.Resolve();
                    break;
                default:
            }
            System.out.println("Try Again? y/n");
            input.nextLine();
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")){
                flag = false;
            }
        }
    }
}