package Warmup;

import InputPlural.Utility;


import java.text.DecimalFormat;
import java.util.Scanner;

public class PlusMinus05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("masukan deret angka = ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        double length = intArray.length;
        double jumlah = 0;
        double jumlah1 = 0;
        double jumlah2 = 0;

        for (int i = 0; i < length; i++) {
            if (intArray[i] > 0){
                jumlah2++;
            }
            else if (intArray[i] == 0){
                jumlah1++;
            }
            else if (intArray[i] < 0){
                jumlah++;
            }
        }

        DecimalFormat desimal = new DecimalFormat("0.0");
        jumlah2 = ((jumlah2/length));
        jumlah = ((jumlah/length));
        jumlah1 = ((jumlah1/length));

        System.out.println("nilai positive = "+ desimal.format(jumlah2));
        System.out.println("nilai negatif = "+ desimal.format(jumlah));
        System.out.println("nilai zero = "+ desimal.format(jumlah1));
    }
}

