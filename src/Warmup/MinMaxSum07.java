package Warmup;

import InputPlural.Utility;

import java.util.Arrays;
import java.util.Scanner;

public class MinMaxSum07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("input deret angka = ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        Arrays.sort(intArray);
        int nilaiMax = 0;
        int nilaiMin = 0;

        for (int i = 1; i < length; i++) {
            nilaiMax += intArray[i];
        }
        for (int i = 0; i < length - 1; i++) {
            nilaiMin += intArray[i];
        }
        System.out.println("nilai maksimal = " + nilaiMax);
        System.out.println("nilai minimal = " + nilaiMin);
    }

}
