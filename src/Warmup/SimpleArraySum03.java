package Warmup;

import InputPlural.Utility;

import java.util.Scanner;

public class SimpleArraySum03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input Deret Angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        int jumlah = 0; // Jumlah nilai suku

        for (int i = 0; i < length; i++) {
            jumlah = jumlah+intArray[i];
        }
        System.out.println(jumlah + " ");
    }
}
