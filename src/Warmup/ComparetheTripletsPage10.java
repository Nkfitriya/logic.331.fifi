package Warmup;

import InputPlural.Utility;

import java.util.Scanner;

public class ComparetheTripletsPage10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukan nilai pertama = ");
        String masukan = input.nextLine();
        System.out.println("masukan nilai kedua = ");
        String masukan2 = input.nextLine();

        int nilaiBob = 0;
        int nilaiAlice = 0;

        int[] deret1 = Utility.ConvertStringToArrayInt(masukan);
        int[] deret2 = Utility.ConvertStringToArrayInt(masukan2);

        for (int i = 0; i < deret1.length; i++) {
            if (deret1[i] > deret2[i]){
                nilaiBob += 1;
            } else if (deret1[i] < deret2[i]) {
                nilaiAlice += 1;
            }
        }

        System.out.println(nilaiAlice + " " + nilaiBob);
    }
}
