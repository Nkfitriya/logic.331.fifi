package Warmup;

import DeretAngka.Utility;

import java.util.Scanner;

public class Staircase06 {

        public static void Resolve() {
            Scanner input = new Scanner(System.in);

            System.out.print("Input n : ");
            int n = input.nextInt();

            int[][] results = new int[n][n];

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if ((j + i) >= n -1 )
                    {
                        results[i][j] = 0;
                    } else
                    {
                        results[i][j] = 1;
                    }
                }
            }
            Utility.Pagar(results);
        }
}
