package Warmup;

import InputPlural.Utility;

import java.util.Scanner;

public class AVeryBigSum09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("masukan deret angka = ");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        long length = intArray.length;
        long  nilai = 0;

        for (int i = 0; i < length; i++) {
            nilai += intArray[i];
        }
        System.out.println("hasil = " + nilai);
    }

}
