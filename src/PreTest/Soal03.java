package PreTest;

import java.util.Scanner;

// si  angka 1

public class Soal03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Inputkan Nilai N = ");
        int n = input.nextInt();

        int[] angka = new int[n];
        int helper = 0;
        int helper1 = 0;

        for (int i = 0; i < n; i++) {
            angka[i] = 100 + helper1;
            helper++;
            helper1 = (int) Math.pow(3, helper);
            System.out.println(angka[i] + " adalah 'Si Angka 1' ");
        }
    }
}
