package PreTest;

import Array2D.Utility;

import java.util.Scanner;

// input n, baris 1 ganjil baris 2 genap. angka sesuai N

public class Soal01 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Nilai N = ");
        int n = input.nextInt();

        int[] results = new int[n];
        int baris1 = -1;
        int baris2 = 0;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n / 2; j++) {
                if (i == 0) {
                    results[i] = baris1;
                    baris1 += 2;
                    System.out.print(baris1 + " ");
                } else if (i == 1) {
                    results[j] = baris2;
                    baris2 += 2;
                    System.out.print(baris2 + " ");
                }

            }
            System.out.println();
        }

    }
}

//        for (int i = 0; i < n; i+=2) {
//        angkaganjil[i] = helper1;
//        helper1 += 2;
//        System.out.print(angkaganjil[i] + " ");
//        }
//        System.out.println();
//
//        for (int i = 0; i < n; i+=2) {
//        angkagenap[i] = helper2;
//        helper2 += 2;
//        if (n % 2 == 0) {
//        if (angkagenap[i] <= n) {
//        System.out.print(angkagenap[i] + " ");
//        }
//        } else if (n % 2 == 1) {
//        if (angkagenap[i] < n) {
//        System.out.print(angkagenap[i] + " ");
//        }
//        }
//        }
//        System.out.println();
//        }
//        }
