package PreTest;

import java.util.Scanner;

// lembah gunung. ninja hatori

public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String rute;
        int ketingggian = 0, gunung = 0, lembah = 0;

        System.out.println("Rute harus huruf kapital dan jangan menggunakan spasi");
        System.out.println("Contoh: NNTTNT");
        System.out.print("Masukan rute: ");
        rute = input.nextLine();

        char[] charRute = rute.toCharArray();

        for (int i = 0; i < charRute.length; i++) {
            if (charRute[i] == 'N'){
                ketingggian++;

                if (ketingggian == 0){
                    lembah++;
                }
            }
            else if (charRute[i] == 'T') {
                ketingggian--;

                if (ketingggian == 0){
                    gunung++;
                }
            }
        }

        System.out.println("Gunung: " + gunung);
        System.out.println("Lembah: " + lembah);
    }
}
