package PreTest;

import java.util.Random;
import java.util.Scanner;

// kartu gphm tp

public class Soal07 {
    public static void Resolve(){
         Scanner input = new Scanner(System.in);
        System.out.println("Mari bermain Kartu!!");
        System.out.print("Masukkan banyak kartu awal = ");
        int banyakN = input.nextInt();

        int player = banyakN;
        int komputer = banyakN;
        boolean main = true;

        // Olah data
        Random random = new Random();
        int count = 1;
        int nilaiA = 0;
        int nilaiB = 0;
        boolean menang = false;
        while(main){
            boolean flagInput = true;
            int tawaran = 0;
            String printOut = "";
            while (flagInput){
                System.out.println("Masukkan Tawaran ke-" + count);
                System.out.println("input : ");
                tawaran = input.nextInt();
                if (tawaran > player || tawaran > komputer){
                    System.out.println("Tawaran tidak boleh lebih dari kartu saat ini" + count);
                }else {
                    flagInput = false;
                }
            }
            System.out.println("Pilih kotak A atau Kotak B");
            //bug
            input.nextLine();
            String pilihan = input.nextLine();
            nilaiA = random.nextInt(9);
            nilaiB = random.nextInt(9);
            if (pilihan.equals("A")){
                if (nilaiA > nilaiB){
                    menang = true;
                    printOut += "Nilai A = " + nilaiA + " YOU WIN";
                }else {
                    menang = false;
                    printOut += "Nilai A = " + nilaiA + " YOU LOSE";
                }
            } else if (pilihan.equals("B")) {
                if (nilaiB > nilaiA){
                    menang = true;
                    printOut += "Nilai B = " + nilaiB + " YOU WIN";
                }else {
                    menang = false;
                    printOut += "Nilai B = " + nilaiB + " YOU LOSE";
                }
            }

            if (menang) {
                player += tawaran;
                komputer -= tawaran;
                System.out.println(printOut);
                System.out.println("Sisa Kartu Kamu = " + player);
                System.out.println("Sisa Kartu Komputer = " + komputer);
            }else {
                player -= tawaran;
                komputer += tawaran;
                System.out.println(printOut);
                System.out.println();
                System.out.println("Sisa Kartu Kamu = " + player);
                System.out.println("Sisa Kartu Komputer = " + komputer);
            }

            if (player <= 0){
                System.out.println("Permainan Selesai, Kamu kalah");
                main = false;
            }else if (komputer <= 0){
                System.out.println("Permainan Selesai, Kamu menang dengan kartu : " + player);
                main = false;
            }

            count++;
        }
    }
}
