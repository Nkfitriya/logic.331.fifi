package PreTest;

import Array2D.Utility;

import java.util.Scanner;

// prima fibonaci jumlah

public class Soal08 {
    public static void Resolve() {
            Scanner input = new Scanner(System.in);
            System.out.println("Masukan nilai n = ");
            int n = input.nextInt();
            int[][] results = new int[3][n];
            int baris1 = 1;
            int baris2 = 1;
            int baris3 = 0;
            int u1 = 0;
            int u2 = 1;

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == 0) {
                        boolean flag = true;
                        while (flag) {
                            if (baris1 == 1) {
                                baris1++;
                            } else if (baris1 == 2) {
                                results[i][j] = baris1;
                                baris1++;
                                flag = false;
                            } else if (baris1 == 3) {
                                results[i][j] = baris1;
                                baris1++;
                                flag = false;
                            } else if (baris1 % 2 != 0 && baris1 % 3 != 0) {
                                results[i][j] = baris1;
                                baris1++;
                                flag = false;
                            } else {
                                baris1++;
                            }
                        }
                    } else if (i == 1) {
                        results[i][j] = baris2;
                        baris2 = u1 + u2;
                        u1 = u2;
                        u2 = baris2;
                    } else if (i == 2) {
                        results[i][j] = results[0][j] + results[1][j];
                    }
                }
            }
            Utility.PrintArray2D(results);
    }
}
