package PreTest;

import ProblemSolve.Utility;

import java.util.Scanner;

// toko
public class Soal04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Perjalanan pengantar makanan");
        System.out.println("Jarak antar tempat");
        System.out.println("1. Toko ke tempat 1 = 2KM");
        System.out.println("2. Tempat1 ke Tempat2 = 500M");
        System.out.println("3. Tempat2 ke Tempat3 = 1,5KM");
        System.out.println("4. Tempat3 ke tempat4 = 2,5KM");
        System.out.println("1 Liter bahan bakar = 2,5 KM jarak tempuh");
        System.out.println("contoh input rute = 1-2-1");
        System.out.println();

        System.out.println("inputkan RUTE = ");
        String rute = input.nextLine();
        String jaraktempat = ("0 2 0.5 1.5 2.5");
        double jaraktempuh =0;
        double selisihjarak =0;

        int[] rutearray = Utility.ConvertStringToArrayInt(rute);
        double[] jaraktempatarray = Utility.ConvertStringToArraydouble(jaraktempat);

        for (int i = 0; i < rutearray.length; i++) {
            if (i==0){
                jaraktempuh = jaraktempatarray[rutearray[i]];
            } else if (i == rutearray.length -1) {
                selisihjarak = jaraktempatarray[rutearray[i]] - jaraktempatarray[rutearray[i -1]];
                if (selisihjarak < 0){
                    selisihjarak *= -1;
                }
                jaraktempuh += (selisihjarak + jaraktempatarray[rutearray[i]]);
            } else {
                selisihjarak =  jaraktempatarray[rutearray[i]] - jaraktempatarray[rutearray[i -1]];
                if (selisihjarak < 0){
                    selisihjarak *= -1;
                }
                jaraktempuh += selisihjarak;
            }

        }
        int banyakbahanbakar =(int) (jaraktempuh / 2.5);
        System.out.println("Bahan bakar yang dibutuhkan = " + banyakbahanbakar + " liter");
    }
}