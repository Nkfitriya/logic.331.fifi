package ProblemSolve;

import java.util.Scanner;

//4. XXXXXX
//        Andi memiliki sejumlah uang dan wishlist di e-commerce.
//        Jika Andi ingin membeli sebanyak mungkin barang dari wishlist-nya,
//        barang apa sajakah yang bisa dibeli Andi dengan uang tersebut?
//        Jika ada barang dengan harga yang sama, yang diprioritaskan adalah yang pertama di-input.
//        Input: Uang Andi, Jumlah barang, nama masing-masing barang dan harganya
//        Output: nama-nama barang yang bisa dibeli
//        Contoh:
//        Uang Andi: 100.000
//        Jumlah barang: 6
//        Nama barang:
//        - Kaos batman, 50.000
//        - Sepatu, 90.000
//        - Dompet, 30.000
//        - Pomade, 50.000
//        - Casing handphone, 20.000
//        - Cologne, 50.000
//
//        Output: Kaos batman, Casing handphone, Dompet
public class Soal04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Uang andi");
        int uang = input.nextInt();
        System.out.println("jumlah barang");
        int banyakbarang = input.nextInt();
        String[] namabarang = new String[banyakbarang];
        int[] hargabarang = new int[banyakbarang];
        input.nextLine();

        for (int i = 0; i < banyakbarang; i++) {
            System.out.println("masukkan nama barang"+(i+1));
            namabarang[i] = input.nextLine();
            System.out.println("masukkan harga barang" + (i +1));
            hargabarang[i] = input.nextInt();
            input.nextLine();
        }
        for (int i = 0; i < banyakbarang; i++) {
            if (uang >= hargabarang[i]){
                System.out.println(namabarang[i]);
                uang = uang - hargabarang[i];
            }

        }
    }
}
