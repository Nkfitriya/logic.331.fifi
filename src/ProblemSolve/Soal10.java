package ProblemSolve;

import java.util.Scanner;

//        10. Jika 1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir.
//        Buatlah sistem konversi volume berdasarkan data di atas
//        Contoh: 1 botol = .. cangkir?
//        1 botol = 5 cangkir

public class Soal10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Ukuran Setiap Wadah");
        System.out.println("1 botol = 2 gelas, 1 teko = 25 cangkir, 1 gelas = 2.5 cangkir");
        System.out.println("Masukkan nama wadah yang ingin di konversikan = ");
        String inputwadah = input.nextLine();

        while (!inputwadah.equals("botol") && !inputwadah.equals("gelas") && !inputwadah.equals("cangkir") && !inputwadah.equals("gelas") && !inputwadah.equals("teko")){
            System.out.println("Wadah Tidak Tersedia");
            System.out.println("Masukkan nama wadah yang ingin di konversikan = ");
            inputwadah = input.nextLine();
        }

        System.out.println("inputkan jumlah wadah = ");
        double jumlahwadah = input.nextInt();

        System.out.println("Masukkan nama wadah konversi = ");
        input.nextLine();
        String inputkonversi = input.nextLine();

        while (!inputkonversi.equals("botol") && !inputkonversi.equals("gelas") && !inputkonversi.equals("cangkir")
                && !inputkonversi.equals("teko")){
            System.out.println("Wadah Konversi tidak tersedia");
            System.out.println("Masukkan nama wadah konversi = ");
            inputkonversi = input.nextLine();
        }

        if (inputwadah.equals("botol")){
            if (inputkonversi.equals("botol")){
                jumlahwadah = jumlahwadah;
            } else if (inputkonversi.equals("gelas")) {
                jumlahwadah = jumlahwadah * 2;
            } else if (inputkonversi.equals("cangkir")) {
                jumlahwadah = jumlahwadah * 5;
            } else if (inputkonversi.equals("teko")) {
                jumlahwadah = jumlahwadah / 5;
            }
        } else if (inputwadah.equals("gelas")) {
            if (inputkonversi.equals("botol")){
                jumlahwadah = jumlahwadah /2;
            } else if (inputkonversi.equals("gelas")) {
                jumlahwadah = jumlahwadah;
            } else if (inputkonversi.equals("cangkir")) {
                jumlahwadah = (double) jumlahwadah * 2.5;
            } else if (inputkonversi.equals("teko")) {
                jumlahwadah = jumlahwadah / 10;
            }
        } else if (inputwadah.equals("cangkir")) {
            if (inputkonversi.equals("botol")){
                jumlahwadah = jumlahwadah / 5;
            } else if (inputkonversi.equals("gelas")) {
                jumlahwadah = (double) jumlahwadah / 2.5;
            } else if (inputkonversi.equals("cangkir")) {
                jumlahwadah = jumlahwadah;
            } else if (inputkonversi.equals("teko")) {
                jumlahwadah = jumlahwadah / 25;
            }
        } else if (inputwadah.equals("teko")) {
            if (inputkonversi.equals("botol")){
                jumlahwadah = jumlahwadah * 5;
            } else if (inputkonversi.equals("gelas")) {
                jumlahwadah = jumlahwadah * 10;
            } else if (inputkonversi.equals("cangkir")) {
                jumlahwadah = jumlahwadah * 25;
            } else if (inputkonversi.equals("teko")) {
                jumlahwadah = jumlahwadah;
            }
        }
        System.out.println("Hasil = " + jumlahwadah +" "+ inputkonversi);
    }
}
