package ProblemSolve;

import java.util.Scanner;

//        Setiap transaksi beli pulsa akan mendapatkan point dengan detail sebagai berikut :
//        0 - 10.000 -> 0 point
//        10.001 - 30.000 -> 1 point setiap kelipatan 1.000
//        > 30.000 -> 2 point setiap kelipatan 1.000
//        Tentukan point yang didapat oleh para pembeli
//        Contoh :
//        Beli pulsa Rp. 20.000
//        0 - 10.000 -> 0 point
//        10.001 - 20.000 -> 10.000 / 1.000  = 10 point
//        Output : 0 + 10 = 10 point
//        Beli pulsa Rp. 75.000
//        0 - 10.000 -> 0 point
//        10.001 - 30.000 -> 20.000 / 1.000  = 20 point
//        30.001 - 75.000 -> (45.000 / 1.000) * 2 = 90 point
//        Output : 0 + 20 + 90 = 110 point

public class Soal11 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int pulsa = 0;
        int jumlahpoint = 0;
//        int point1;
//        int point2;

        System.out.println("Masukkan jumlah beli Pulsa = ");
        pulsa = input.nextInt();

        if (pulsa < 10000){
            jumlahpoint = 0;
        } else if (pulsa > 10000 && pulsa < 30000) {
            jumlahpoint = (pulsa - 10000 ) / 1000;
        } else if (pulsa >= 30000){
            jumlahpoint = (pulsa - 30000) / 500;
            jumlahpoint +=20;
        }
        System.out.println(jumlahpoint + " point");
    }
}
