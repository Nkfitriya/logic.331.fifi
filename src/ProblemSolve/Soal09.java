package ProblemSolve;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

//        9.XXXXXXX
//        Buatlah fungsi untuk kalkulasi tarif parkir berdasarkan ketentuan berikut
//        Ketentuan tarif:
//        1. Delapan jam pertama: 1.000/jam
//        2. Lebih dari 8 jam s.d. 24 jam: 8.000 flat
//        3. Lebih dari 24 jam: 15.000/24 jam dan selebihnya mengikuti ketentuan pertama dan kedua
//        Input:
//        - Tanggal dan jam masuk
//        - Tanggal dan jam keluar
//        Output: besarnya tarif parkir
//        Contoh: -Masuk: 28 Januari 2020 07:30:34
//        -Keluar: 28 Januari 2020 20:03:35
//        Output: 8000
//        Penjelasan: Lamanya parkir adalah 12 jam 33 menit 1 detik, sehingga perhitungan tarif parkir dibulatkan menjadi 13 Jam.
//        Mengacu pada ketentuan kedua, maka yang harus dibayarkan adalah 8000 rupiah.


public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int hari = 0;
        int biayaparkir = 0;
        Date waktumasukparkir = null, waktukeluarparkir = null;

        SimpleDateFormat waktu = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        System.out.println("Masukkan Tanggal Parkir dan waktu parkirnya = ");
        String masukparkir = input.nextLine();

        System.out.println("Masukkan Tanggal Keluar dan waktu keluarnya = ");
        String keluarparkir = input.nextLine();

        try {
            waktumasukparkir = waktu.parse(masukparkir);
        } catch (ParseException e) {
            System.out.println("Format Waktu Salah");;
        }

        try {
            waktukeluarparkir = waktu.parse(keluarparkir);
        } catch (ParseException e) {
            System.out.println("Format Waktu Salah");;
        }

        long waktuparkir = waktukeluarparkir.getTime() - waktumasukparkir.getTime();
        long convertJamParkir = waktuparkir / (1000 * 60 * 60);
        System.out.println("Waktu Parkir = " + convertJamParkir);

        if (convertJamParkir <= 8){
            biayaparkir = (int) convertJamParkir * 1000;
            System.out.println("Biaya Parkir = " +  biayaparkir);
        } else if (convertJamParkir > 8 && convertJamParkir <= 24) {
            biayaparkir = 8000;
            System.out.println("Biaya Parkir = " + biayaparkir);
        }else {
            hari = (int) convertJamParkir / 24;
            biayaparkir = hari * 15000;
            System.out.println("Biaya Parkir = " + biayaparkir);

        }
    }
}
