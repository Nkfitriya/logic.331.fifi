package ProblemSolve;

import java.util.Scanner;

//1. Buatlah deret angka yang terbentuk dari penjumlahan deret bilangan kelipatan 3 dikurang 1
//        dan deret bilangan kelipatan (-2) x 1. Angka pada index ganjil dari kedua deret bilangan
//        tersebut saling dijumlahkan. Dan angka pada index genap dari kedua deret bilangan tersebut
//        juga saling dijumlahkan. Index dimulai dari angka 0.
//        Input : Panjang array/panjang deret
//        Contoh : Dibawah ini hanya sekedar contoh yang menggunakan deret genap dan ganjil
//
//        Input panjang deret : 5
//        Deret genap : 0 2 4 6 8
//        Deret ganjil : 1 3 5 7 9
//        0 + 1 ; 2 + 3 ; 4 + 5 ; 6 + 7 ; 8 + 9
//
//        Output : 1, 5, 9, 13, 17
//
//        Answer :
//        input - proses - output
//        index dimulai dari 0. 3-1 = 2, (-2)x1 = -2

public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Nilai N = ");
        int n = input.nextInt();

        int[] deret1 = new int[n];
        int[] deret2 = new int[n];
        int[] output = new int[n];

        System.out.print("Deret 1 = ");
        for (int i = 0; i < deret1.length ; i++) {
            deret1[i] =(i * 3)  - 1;
            System.out.print(deret1[i] + " ");
        }
        System.out.println();
        System.out.print("Deret 2 = ");
        for (int i = 0; i < deret2.length; i++) {
            deret2[i] = (i * (-2)) * 1;
            System.out.print(deret2[i] + " ");
        }
        System.out.println();
        System.out.print("Output = ");
        for (int i = 0; i < output.length; i++) {
            output[i] = deret1[i] + deret2[i];
            System.out.print(output[i] + " ");
        }
        System.out.println();

    }
}
