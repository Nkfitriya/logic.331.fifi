package ProblemSolve;

import java.util.Scanner;

//        5.XXXXXX
//        Dengan hanya menggunakan logic, ubah format jam dari 24H ke 12H dan juga sebaliknya
//        contoh:
//        input: 12:35 AM
//        output: 00:35
//        input: 19:30
//        output: 07:30 PM
//        input: 09:05
//        output: 09:05 AM
//        input: 11:30 PM
//        output: 23:30
//        NB: perbedaan format 12H dan 24H ada pada jam 00 tengah malam
//        (jam 00 adalah jam 12 AM, lihat contoh 1), selebihnya tinggal
//        menambahkan PM antara jam 12:00 - 23:59 dan AM antara jam 00:00 - 11:59

public class Soal05 {

   public static void Resolve(){
      Scanner input = new Scanner(System.in);
      String format;
      String time;
      int jam;
      int menit;

      System.out.println("masukan jam = ");
      time = input.nextLine();

       if (time.length() == 5) {
           String[] times = time.split(":");

           jam = Integer.parseInt(times[0]);
           menit = Integer.parseInt(times[1]);

           if (jam > 12) {
               System.out.println(jam - 12 + ":" + menit + " PM");
           } else {
               System.out.println(jam + ":" + menit + " AM");
           }
       }
       else if (time.length() > 5) {
           String[] times = time.split(":");

           jam = Integer.parseInt(times[0]);
           menit = Integer.parseInt(times[1].substring(0, 2));
           format = times[1].substring(2, 4);

           if (format.equalsIgnoreCase("PM") || format.equalsIgnoreCase("P")){
               System.out.println(jam + 12 + ":" + menit);
               }
           else {
                   System.out.println(jam - 12 + ":" + menit);
               }
       }

    }
}