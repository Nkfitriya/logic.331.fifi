package ProblemSolve;

import java.util.Arrays;
import java.util.Scanner;

//      8. Andi memiliki sejumlah uang. Dengan uang itu, ia bermaksud membeli sebuah kacamata
//        dan sepotong baju. Jika Andi bermaksud menggunakan uangnya semaksimal mungkin,
//        tentukan berapa uang yang dapat Andi belanjakan. Jika harga kedua barang di luar jangkauan Andi,
//        keluarkan pesan "Dana tidak mencukupi".
//        Input :
//        Terdiri dari 1 integer (jumlah uang yang dimiliki Andi) dan 2 baris array yang
//        masing-masing berisi 3 data harga kacamata dan harga baju
//        Contoh :
//        Uang Andi: 70
//        Harga kacamata: 34, 26, 44
//        Harga baju: 21, 39, 33
//        Output = 67
//        Constrains :
//        - Uangnya hanya 70 dan akan dipakai semaksimal mungkin (sum <= 70)
//        - Ada 2 item yang akan dibeli (baju + kacamata)


public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan uang yang andi miliki");
        int uang = input.nextInt();

        System.out.println("contoh. 45, 23, 30");
        System.out.println("masukkan harga kacamata = ");
        input.nextLine();
        String hargakacamata = input.nextLine();
        String[] hargakacamatasplit = hargakacamata.split(", ");
        int [] hargakacamataarray = new int[hargakacamatasplit.length];

        for (int i = 0; i < hargakacamatasplit.length; i++) {
            hargakacamataarray[i] = Integer.parseInt(hargakacamatasplit[i]);
        }

        System.out.println("contoh. 45, 23, 30");
        System.out.println("masukkan harga baju = ");
        String hargabaju = input.nextLine();
        String[] hargabajusplit = hargabaju.split(", ");
        int[] hargabajuarray = new int[hargabajusplit.length];

        for (int i = 0; i < hargabajusplit.length; i++) {
            hargabajuarray[i] = Integer.parseInt(hargabajusplit[i]);
        }

        int[] jumlahharga = new int[hargakacamataarray.length * hargabajuarray.length];
        int helper = 0;

        for (int i = 0; i < hargakacamataarray.length; i++) {
            for (int j = 0; j < hargabajuarray.length; j++) {
                jumlahharga[helper] = hargakacamataarray[i] + hargabajuarray[j];
                helper++;
            }

        }
        Arrays.sort(jumlahharga);
        int helperjumlahhargabarang = 0;
        for (int i = 0; i < jumlahharga.length ; i++) {
            if (jumlahharga[i] <= uang){
                helperjumlahhargabarang = jumlahharga[i];
            }
        }
        System.out.println("harga barang tertinggi yang bisa dibeli = " + helperjumlahhargabarang);
    }
}