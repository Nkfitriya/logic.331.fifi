package ProblemSolve;

import java.util.Scanner;

//        7. XXXXXXXX
//        Jim diminta untuk menyusuri lintasan linier dengan lubang-lubang yang harus dilompati.
//        Jika Jim memilih untuk berjalan, dia akan berpindah tempat sebesar 1 dan mendapatkan energi
//        sebesar 1, tetapi jika dia melompat dia akan berpindah tempat sebesar 2 dan kehilangan energi sebesar 2.
//        Di awal, energi Jim adalah 0. Dengan kombinasi jalan-lompat yang kamu pilih, apakah Jim mampu melewati
//        lintasan itu? Jika mampu, berapakah energi akhirnya? (jika tidak mampu, tulis saja \'Jim died\').
//        Lubangnya selalu berada di antara jalan, tidak ada lubang berturut-turut.
//        Jim boleh melompat kapanpun, tidak harus pada lubang saja.
//        Jika Jim salah memperhitungkan lompat/jalan, dia bisa masuk ke lubang dan mati.
//        Input:
//        1. Pola lintasan (- melambangkan jalan, o melambangkan lubang)
//        2. Pilihan cara jalan (w jalan, j lompat)
//        Output: energi akhi
//        Contoh:
//        1. ---o-o-
//        2. wwwjj
//        3. Jim died (Jim tidak punya tenaga untuk melompati lubang kedua)
//        1. -----o----o----o-
//        2. wwwwwjwwwjwwwj
//        3. 5
//        1. -----o----o----o-
//        2. wwwwwjjwjwwwj
//        3. 1
//        1. -----o----o----o-
//        2. wwjwwj
//        3. Jim died (setelah lompatan pertama, Jim tidak punya tenaga untuk melewati lubang pertama)
//        1. -----o----o----o-
//        2. wwwwjwwjwj
//        3. Jim died (lompatan pertamanya mengakibatkan Jim jatuh tepat di lubang pertama)

public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("- melambang jalan");
        System.out.println("o melambangkan lubang");
        System.out.println("ex. inputan = ----o----o---o-");
        System.out.println("Inputkan Lintasan");
        String lintasan = input.nextLine();

        System.out.println("w melambangkan jalan");
        System.out.println("j melambangkan lompat");
        System.out.println("ex. inputkan : wwwwwjwwwjwwwj");
        System.out.println("Inputkan Cara Berpindah");
        String pindah = input.nextLine();

        int energi = 0;
        boolean jatuh = false;
        int helper = 0;

        for (int i = 0; i < pindah.length(); i++) {
            if (pindah.charAt(i) == 'w' && lintasan.charAt(helper) == '-'){
                energi++;
                helper++;
            } else if (pindah.charAt(i) == 'w' && lintasan.charAt(helper) == 'o') {
                jatuh = true;
                break;
            } else if (pindah.charAt(i) == 'j' && energi >=2) {
                helper +=2;
                if (lintasan.charAt(helper -1)== 'o'){
                    jatuh = true;
                    break;
                } else {
                    energi -= 2;
                }
            } else if (pindah.charAt(i) == 'j' && pindah.charAt(helper) == 'o' && energi <2) {
                jatuh = true;
                break;
            }
        }
        if (jatuh){
            System.out.println("Jim Died");
        }
        if (!jatuh){
            System.out.println(energi);
        }
    }
}