package ProblemSolve;

import java.util.Scanner;

//        Huruf alfabet dalam huruf kecil di bawah ini mengandung bobot yang sudah ditentukan sebagai berikut:
//        a = 1; b = 2; c = 3; d = 4; .... Z = 26.
//        Tentukan apakah dalam sebuah input string sudah memiliki bobot yang sesuai.
//        Constraint :
//        -    0 <= n <= 100
//        -    string hanya mengandung huruf kecil
//        Input
//        string : mengandung kata/kalimat
//        array n : mengandung array angka yang harus dicocokkan terhadap string
//        Example
//        string : abcdzzz
//        array : [1, 2, 2, 4, 4, 26, 26]
//        Output : true, true, false, true, false, true, true
//        Explanation :
//        a = 1 -> true
//        b = 2 - > true
//        c = 3 -> false
//        d = 4 -> true
//        z = 4 -> false
//        z = 26 -> true
//        z = 26 -> true


public class Soal13 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kalimat");
        System.out.println("ex.abcdzzz ");
        String kalimat = input.nextLine();

        char[] charkalimat = kalimat.toCharArray();

        System.out.println("Masukkan Angka");
        System.out.println("ex. 1, 2, 2, 4, 4, 26, 26");
        String angka = input.nextLine();
        String[] angkaarray = angka.split(", ");
        int[] angkasplit = new int[angkaarray.length];
        for (int i = 0; i < angkasplit.length; i++) {
            angkasplit[i] = Integer.parseInt(angkaarray[i]);
        }

        String[] results = new String[charkalimat.length];

        for (int i = 0; i < charkalimat.length ; i++) {
            if (charkalimat[i] - 96 == angkasplit[i]){
              results[i] = "true";
            } else {
                results[i] = "false";
            }
        }
        for (int i = 0; i < charkalimat.length; i++) {
            System.out.print(results[i] + ", ");
        }
    }
}
