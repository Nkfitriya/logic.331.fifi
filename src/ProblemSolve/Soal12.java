package ProblemSolve;

import java.util.HashMap;
import java.util.Scanner;

//        Berikut ini adalah record penjualan buah dalam bentuk string
//
//        Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1
//
//        Buat summary penjualannya
//
//        Input: string record penjualan
//        Output: Summary penjualan, dalam alphabetical order
//        Apel: 9
//        Jeruk: 9
//        Mangga: 1
//        Pisang: 3

public class Soal12 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Record Penjualan Buah");
        System.out.println("ex. Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1");
        System.out.println("Masukkan Record penjualan");
        String data = input.nextLine();
        HashMap<String, Integer> buah = new HashMap<String, Integer>();
        int helper = 0;

        String[] datasplit = data.split(", ");
        String[] item = new String[2];

        for (int i = 0; i < datasplit.length; i++) {
            item = datasplit[i].split(":");
            if (buah.get(item[0]) == null){
                buah.put(item[0], Integer.parseInt(item[1]));
            }
            else {
                helper = buah.get(item[0]);
                buah.put(item[0], Integer.parseInt(item[1]) + helper);
            }
        }
        for (String key : buah.keySet()){
            System.out.println(key + ":" + buah.get(key));
        }

    }
}
