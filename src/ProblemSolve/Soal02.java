package ProblemSolve;

import java.util.Scanner;

//        2. Bambang adalah karyawan grosir X yang bertugas mengantarkan barang ke toko-toko
//        pelanggannya yang terletak di satu ruas jalan panjang yang sama, dengan jarak
//        masing-masing sebagai berikut (dihitung dari grosir X):
//        Toko 1: 0.5 km
//        Toko 2: 2 km
//        Toko 3: 3.5 km
//        Toko 4: 5 km
//
//        Jika Bambang menghabiskan waktu rata-rata 10 menit di setiap toko,
//        dan laju rata-rata motor Bambang adalah 30 km/jam, berapa lama waktu yang
//        dibutuhkan Bambang untuk mengantarkan pesanan hingga kembali ke grosir?
//
//        Input: 1-2-3-4
//        Output: 60 menit
//        clue: jarak total adalah 10 km
//
//        Input: 1-3-2-4-1
//        Output: 76 menit
//        clue: jarak total adalah 13 km

public class Soal02 {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Jarak Antara Grosir X ke Toko");
        System.out.println("Toko 1 = 0.5 km");
        System.out.println("Toko 2 = 2 km");
        System.out.println("Toko 3 = 3.5 km");
        System.out.println("Toko 4 = 5 km");
        System.out.println();

        System.out.println("Masukkan Rute yang di inginkan = ");
        String masukkanrute = input.nextLine();
        String jaraktoko = ("0 0.5 2 3.5 5");
        double jaraktempuh = 0;
        double selisihjarak = 0;
        double selisihtempuh = 0;
        int waktu;

        int[] toko = Utility.ConvertStringToArrayInt(masukkanrute);
        double[] jaraktokoarray = Utility.ConvertStringToArraydouble(jaraktoko);


        for (int i = 0; i < toko.length ; i++) {
            if (i == 0){
                // menghitung jarak dari x ke toko pertama yang dikunjungi
                jaraktempuh = jaraktokoarray[toko[i]];
            } else if (i == toko.length -1) {
                selisihjarak = jaraktokoarray[toko[i]] - jaraktokoarray[toko[i - 1]];
                if (selisihjarak < 0){
                    selisihjarak *= -1;
                }
                jaraktempuh += (selisihjarak + jaraktokoarray[toko[i]]);
            } else {
                selisihjarak = jaraktokoarray[toko[i]] - jaraktokoarray[toko[i -1]];
                if (selisihjarak < 0){
                    selisihjarak *= -1;
                }
                jaraktempuh += selisihjarak;
            }

        }
        waktu = (int) ((jaraktempuh * 2) + (toko.length * 10));
        System.out.println("Waktu yang dibutuhkan = " + waktu + "Menit");
        System.out.println("Jarak yang ditempuh = " + jaraktempuh + "KM");
    }
}
