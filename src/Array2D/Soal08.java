package Array2D;

import java.util.Scanner;

// Soal 08
// n = 7
// 0	1	2	3	4	5	6
// 0	2	4	6	8	10	12
// 0	3	6	9	12	15	18

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nilai N = ");
        int n = input.nextInt();

        int[][] results = new int[3][n];
        int tambah1 = 0;
        int tambah2 = 0;
        int tambah3 = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = tambah1;
                    tambah1 += 1;
                }else if (i == 1) {
                    results[i][j] = tambah2;
                    tambah2 +=2;
                } else if (i == 2) {
                    results[i][j] = tambah3;
                    tambah3 +=3;
                }

            }

        }
        Utility.PrintArray2D(results);
    }
}
