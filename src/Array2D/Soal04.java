package Array2D;

import java.util.Scanner;

// Soal 04
// n = 7, n2 = 5
// 0	1	2	3	4	5	6
// 1	5	2	10	3	15	4

public class Soal04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Nilai n =");
        int n = input.nextInt();

        int[][] results = new int[2][n];
        int tambah = 0;
        int tambah1 = 1;
        int tambah2 = 5;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = tambah;
                    tambah += 1;
                } else if (i == 1) {
                    if ((j+1) % 2 == 1){
                        results[i][j]= tambah1;
                        tambah1++;
                    } else {
                        results[i][j] = tambah2;
                        tambah2 +=5;
                    }

                }

            }

        }
        Utility.PrintArray2D(results);
  }
}
