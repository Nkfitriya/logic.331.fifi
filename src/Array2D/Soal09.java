package Array2D;

import java.util.Scanner;

// Soal 09
// n = 7, n2 = 3
// 0	1	2	3	4	5	6
// 0	3	6	9	12	15	18
// 18	15	12	9	6	3	0


public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nilai N = ");
        int n = input.nextInt();

        int[][] results = new int[3][n];
        int tambah1 = 0;
        int tambah3 = 0;
        int kurang3 = (n-1)*3;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = tambah1;
                    tambah1 +=1;
                } else if (i == 1) {
                    results[i][j] = tambah3;
                    tambah3 +=3;
                } else if (i ==2) {
                    results[i][j] = kurang3;
                    kurang3 -=3;
                }

            }

        }
        Utility.PrintArray2D(results);
    }
}
