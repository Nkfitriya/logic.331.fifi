package Array2D;

import java.util.Scanner;

// Soal 02
// n = 7, n2 = 3
// 0	1	2	3	4	5	6
// 1	3	-9	27	81	-243	729

public class Soal02 {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Nilai N = ");
        int n = input.nextInt();

        int[][] results = new int[2][n];
        int tambah = 0;
        int kali = 1;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = tambah;
                    tambah+=1;
                } else if (i == 1){
                    if ((j + 1) % 3 == 0) {
                        kali *= -1;
                        results[i][j] = kali;
                        kali *= -3;
                    } else {
                        results[i][j] = kali;
                        kali *= 3;
                    }
                }

            }

        }
        Utility.PrintArray2D(results);
    }
}
