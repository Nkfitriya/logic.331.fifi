package Array2D;

import java.util.Scanner;

//Soal 12 (adv, n only odd number)
// n = 7==
//              0
//          0	1	0
//      0	1	0	1	0
// 0	1	0	1	0	1	0


public class Soal12 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan Nilai N = ");
        int n = input.nextInt();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - i; j++) {
                System.out.print("	");
            }

            for (int k = 0; k <= i * 2; k++) {
                if (k % 2 == 0) {
                    System.out.print("0	");
                } else {
                    System.out.print("1	");
                }
            }

            System.out.println();
        }
    }
}
