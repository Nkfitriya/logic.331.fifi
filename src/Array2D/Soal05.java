package Array2D;

import java.util.Scanner;

// Soal 05
// n = 7
// 0	1	2	3	4	5	6
// 7	8	9	10	11	12	13
// 14	15	16	17	18	19	20

// n = 5
// 0	1	2	3	4
// 5	6	7	8	9
// 10	11	12	13	14

public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan nilai N = ");
        int n = input.nextInt();

        int[][] results = new int[3][n];
        int baris1  = 0;
        int baris2 = n;
        int baris3 = baris2 + n;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = baris1;
                    baris1 ++;
                } else if (i == 1){
                    results[i][j] = baris2;
                    baris2 ++;
                } else if (i == 2) {
                    results[i][j] = baris3;
                    baris3 ++;
                }

            }

        }
        Utility.PrintArray2D(results);
    }
}
