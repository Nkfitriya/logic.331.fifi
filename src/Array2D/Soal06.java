package Array2D;

import java.util.Scanner;

// Soal 06
// n = 7
// 0	1	2	3	4	5	6
// 1	7	49	343	2401	16807	117649
// 1	8	51	346	2405	16812	117655
//
// n = 5
// 0	1	2	3	4
// 1	5	25	125	625
// 1	6	27	128	629


public class Soal06 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("MAsukkan nilai N =");
        int n = input.nextInt();

        int[][] results = new int[3][n];
        int tambah = 0;
        int kali = 1;
        int help1 = 0;
        int help2 = 1;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = tambah;
                    tambah +=1;
                } else if (i == 1) {
                    results[i][j] = kali;
                    kali *=n;
                } else if (i == 2) {
                    results[i][j] = help1 + help2;
                    help1++;
                    help2 *=n;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
