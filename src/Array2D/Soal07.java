package Array2D;

import java.util.Scanner;

// Soal 07==
// n = 7
// 0	1	2	-3	4	5	-6
// 7	8	-9	10	11	-12	13
// 14	-15	16	17	-18	19	20


public class Soal07 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Nilai N =");
        int n = input.nextInt();

        int[][] results = new int[3][n];
        int baris1  = 0;
        int baris2 = baris1+ n;
        int baris3 = baris2 + n;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    if ((baris1) % 3 == 0) {
                        baris1 *= -1;
                        results[i][j] = baris1;
                        baris1 = (baris1 * -1) +1;
                    } else {
                        results[i][j] =baris1;
                        baris1++;
                    }
                }
                else if (i == 1){
                    if ((baris2) % 3 == 0){
                        baris2 *=  -1;
                        results[i][j] = baris2;
                        baris2 = (baris2 * -1) + 1;
                    } else {
                        results[i][j] = baris2;
                        baris2++;
                    }
                } else if (i == 2) {
                    if ((baris3) % 3 ==0){
                        baris3 *= -1;
                        results[i][j] =baris3;
                        baris3 = (baris3 * -1) + 1;
                    } else {
                        results[i][j] = baris3;
                        baris3++;
                    }
                }

            }

        }
        Utility.PrintArray2D(results);
    }
}
