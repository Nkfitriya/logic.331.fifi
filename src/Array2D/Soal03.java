package Array2D;


import java.util.Scanner;

//  Soal 03
//  n = 7, n2 = 3
//  0	1	2	3	4	5	6
//  3	6	12	24	12	6	3

public class Soal03 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Nilai N = ");
        int n = input.nextInt();

        int[][] results = new int[2][n];
        int tambah = 0;
        int tambahkelipatan = 3;

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    results[i][j] = tambah;
                    tambah ++;
                } else if ( i == 1){
                    if (n % 2 != 0 ) { // jika n modulus 2 tidak bersisa 0 berarti ganjil
                        if(j < (n/2)){
                            results[i][j] = tambahkelipatan;
                            tambahkelipatan *= 2;
                        }
                        else if (j == (n/2)) {
                            results[i][j] = tambahkelipatan;
                        }
                        else if (j > (n/2)) {
                            tambahkelipatan /= 2;
                            results[i][j] = tambahkelipatan;
                        }
                    }else{
                        if(j < (n/2) -1){
                            results[i][j] = tambahkelipatan;
                            tambahkelipatan *= 2;
                        }
                        else if (j == n/2 || j == (n/2) -1) {
                            results[i][j] = tambahkelipatan;
                        }
                        else if (j > (n/2) -1) {
                            tambahkelipatan /= 2;
                            results[i][j] = tambahkelipatan;
                        }
                    }

                }

            }

        }
        Utility.PrintArray2D(results);

    }
}
