package BangunDatar;

import java.util.Scanner;

public class Persegi {
    private static Scanner input = new Scanner(System.in);
    private static double sisi;

    public static void Luas()
    {
        // Luas Persegi
        System.out.println("Input Panjang Sisi (cm)");
        sisi = input.nextDouble();

        double luas_persegi = sisi * sisi;

        System.out.println("Luas Persegi adalah " + luas_persegi);
    }
    public static void Keliling()
    {
        //Keliling Persegi
        System.out.println("Input Panjang Sisi (cm)");
        sisi = input.nextDouble();


        double keliling_persegi = sisi * 4;

        System.out.println("Keliling Persegi adalah " + keliling_persegi);
    }
}
