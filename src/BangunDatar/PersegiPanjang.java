package BangunDatar;

import java.util.Scanner;

public class PersegiPanjang {

    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;

    public static void Luas()
    {
        //Luas Persegi panjang
        System.out.println("Input Panjang Persegi (cm)");
        panjang = input.nextInt();

        System.out.println("Input Lebar Persegi (cm)");
        lebar = input.nextInt();

        int luas = panjang * lebar;

        System.out.println("Luas Persegi Panjang Adalah " + luas);
    }
    public static void Keliling()
    {
        //Keliling Persegi panjang
        System.out.println("Input Panjang Persegi (cm)");
        panjang = input.nextInt();

        System.out.println("Input Lebar Persegi (cm)");
        lebar = input.nextInt();

        int keliling = 2 * ( panjang + lebar);

        System.out.println("Luas Persegi Panjang Adalah " + keliling);
    }
}
