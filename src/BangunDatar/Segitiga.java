package BangunDatar;

import java.util.Scanner;

public class Segitiga {

    private static Scanner input = new Scanner(System.in);
    private static double tinggi;
    private static double alas;
    private static double sisi_miring;

    public static void Luas()
    {
        //        Luas Segitiga
        System.out.println("Input Panjang Tinggi Segitiga (cm)");
        tinggi = input.nextDouble();

        System.out.println("Input Panjang Alas Segitiga (cm)");
        alas = input.nextDouble();

        System.out.println("Input Panjang Sisi Miring Segitiga (cm)");
        sisi_miring = input.nextDouble();

        double luas_segitiga = 0.5 * alas * tinggi;

        System.out.println("Luas Segitiga adalah " + luas_segitiga);
    }
    public static void Keliling()
    {
        //        Keliling Segitiga
        System.out.println("Input Panjang Tinggi Segitiga (cm)");
        tinggi = input.nextDouble();

        System.out.println("Input Panjang Alas Segitiga (cm)");
        alas = input.nextDouble();

        System.out.println("Input Panjang Sisi Miring Segitiga (cm)");
        sisi_miring = input.nextDouble();

        double keliling_segitiga = tinggi + alas + sisi_miring;

        System.out.println("Keliling Segitiga adalah " + keliling_segitiga);
    }

}
