package BangunDatar;

import java.util.Scanner;

public class Trapesium {

    private static Scanner input = new Scanner(System.in);
    private static double sisi_atas;
    private static double sisi_bawah;
    private static double sisi_kanan;
    private static double sisi_kiri;
    private static double tinggi;

    public static void Luas()
    {
        //        Luas Trapesium
        System.out.println("Input Panjang Sisi Atas Trapesium (cm)");
        sisi_atas = input.nextDouble();

        System.out.println("Input Panjang Sisi Bawah Trapesium (cm)");
        sisi_bawah = input.nextDouble();

        System.out.println("Input Panjang Sisi Miring Sebelah Kanan Tapesium (cm)");
        sisi_kanan = input.nextDouble();

        System.out.println("Input Panjang Sisi Miring Sebelah Kiri Tapesium (cm)");
        sisi_kiri = input.nextDouble();

        System.out.println("Input Panjang Tinggi Tapesium (cm)");
        tinggi = input.nextDouble();

        double luas_trapesium = 0.5 * (sisi_atas + sisi_bawah) * tinggi;

        System.out.println("Luas Trapesium adalah " + luas_trapesium);
    }
    public static void Keliling()
    {
        //        Keliling Trapesium
        System.out.println("Input Panjang Sisi Atas Trapesium (cm)");
        sisi_atas = input.nextDouble();

        System.out.println("Input Panjang Sisi Bawah Trapesium (cm)");
        sisi_bawah = input.nextDouble();

        System.out.println("Input Panjang Sisi Miring Sebelah Kanan Tapesium (cm)");
        sisi_kanan = input.nextDouble();

        System.out.println("Input Panjang Sisi Miring Sebelah Kiri Tapesium (cm)");
        sisi_kiri = input.nextDouble();

        System.out.println("Input Panjang Tinggi Tapesium (cm)");
        tinggi = input.nextDouble();

        double keliling_trapesium = sisi_atas + sisi_bawah + sisi_kanan + sisi_kiri;

        System.out.println("Keliling Trapesium adalah " + keliling_trapesium);
    }
}
