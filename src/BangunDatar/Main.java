package BangunDatar;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
//        String name = "Nikmatul Fitriya";
//        String name2 = "Fifi";
//        int age = 22;
//
//        System.out.println("Hello There!!");
//        System.out.println("My Name is " + name);
//        System.out.println("You can call me " + name2);
//        System.out.println("I am " + age + " years old");

        Scanner input = new Scanner(System.in);

        boolean flag = true;
        String answer = "y";

        while (flag) {
            System.out.println("Pilih Bangun Datar");
            System.out.println("1. Persegi Panjang");
            System.out.println("2. Segitiga");
            System.out.println("3. Trapesium");
            System.out.println("4. Lingkaran");
            System.out.println("5. Persegi");

            int pilihan1 = input.nextInt();
            int pilihan2;

            String prompt = "Pilihan   1.Luas   2.Keliling";

            switch (pilihan1) {
                case 1:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        PersegiPanjang.Luas();
                    } else if (pilihan2 == 2) {
                        PersegiPanjang.Keliling();
                    } else {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                case 2:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1){ Segitiga.Luas();
                    } else if (pilihan2 == 2) {
                        Segitiga.Keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 3:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Trapesium.Luas();
                    } else if (pilihan2 == 2) {
                        Trapesium.Keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 4:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                       Lingkaran.Luas();
                    } else if (pilihan2 == 2) {
                        Lingkaran.Keliling();
                    }else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 5:
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1) {
                        Persegi.Luas();
                    } else if (pilihan2 == 2) {
                        Persegi.Keliling();
                    }else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                default:
                    System.out.println("Pilihan Tidak Tersedia");
            }
            System.out.println("Try Again? y/n");
            input.nextLine(); //skip
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y"))
            {
                flag = false;
            }
        }

    }
}