package BangunDatar;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);
    private static double jari_jari;
    private static double phi;

    public static void Luas()
    {
        //        Luas Lingkaran
        System.out.println("Input panjang Jari-jari (r) lingkaran (cm)");
        jari_jari = input.nextDouble();

        phi = 3.14;
        double luas_lingkaran = phi * jari_jari * jari_jari;

        System.out.println("Luas Lingkaran adalah " + luas_lingkaran);
    }
    public static void Keliling()
    {
        //        Keliling Lingkaran
        System.out.println("Input panjang Jari-jari (r) lingkaran (cm)");
        jari_jari = input.nextDouble();

        phi = 3.14;
        double keliling_lingkaran = 2 * phi * jari_jari;

        System.out.println("Keliling Lingkaran adalah " + keliling_lingkaran);
    }
}