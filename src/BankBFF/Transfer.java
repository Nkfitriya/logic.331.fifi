package BankBFF;

import java.util.Scanner;

public class Transfer {
    private static Scanner input = new Scanner(System.in);

    private static int saldo = 50000000;
    private static boolean flag = true;
    private static String answer = "y";
    private static String rekening;

    public static void Transfer(){

        System.out.println("Pilih Transfer : ");
        System.out.println("1. Antar Rekening");
        System.out.println("2. Antar Bank");
        int pilihan = input.nextInt();
        System.out.println();

        while (flag) {
            switch (pilihan) {
                case 1:
                    System.out.print("Masukkan Nomor Rekening yang dituju: ");
                    input.nextLine();
                    rekening = input.nextLine();

                    if (rekening.length() != 7) {
                        System.out.println("Nomor Rekening Anda tidak Sesuai, silahkan masukkan kembali");
                    } else {
                        System.out.print("Jumlah Uang yang ingin di Transfer : ");
                        int uang = input.nextInt();

                        if (uang >= saldo) {
                            System.out.println("Saldo Anda Tidak Cukup");
                        } else {
                            System.out.println("Transaksi Anda Berhasil");
                            int sisasaldos = saldo - uang;
                            System.out.println("Sisa Saldo Anda Saat ini " + sisasaldos);
                        }
                    }System.out.println("Apakah anda Ingin transaksi Kembali? (y/n)");
                    input.nextLine(); //for skip
                    answer = input.nextLine();

                    if (answer.toLowerCase().equals("y")) {
                        flag = false;
                        Pin.BuatPin();
                    } else {
                        flag = false;
                        break;
                    }

                    break;
                case 2:
                    System.out.print("Masukkan Nomor Rekening yang dituju : ");
                    input.nextLine();
                    rekening = input.nextLine();


                    if (rekening.length() != 10) {
                        System.out.println("Nomor Rekening Anda tidak Sesuai, silahkan masukkan kembali");
                    } else {
                        System.out.print("Jumlah Uang yang ingin di Transfer : ");
                        int uang = input.nextInt();

                        if (uang >= saldo) {
                            System.out.println("Saldo Anda Tidak Cukup");
                        } else {
                            System.out.println("Transaksi Berhasil");
                            int sisaSaldo = saldo - uang;
                            System.out.println("Sisa Saldo Anda Saat ini " + sisaSaldo);
                        }
                    }
                    System.out.println("Apakah anda Ingin transaksi Kembali? (y/n)");
                    input.nextLine(); //for skip
                    answer = input.nextLine();

                    if (answer.toLowerCase().equals("y")) {
                        flag = false;
                        Pin.BuatPin();
                    } else {
                        flag = false;
                        break;
                    }
                    break;
                default:
            }
        }
    }
}
