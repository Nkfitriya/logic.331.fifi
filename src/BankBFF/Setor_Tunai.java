package BankBFF;

import java.util.Scanner;

public class Setor_Tunai {
    private static Scanner input = new Scanner(System.in);

    private static String jumlah;
    private static int saldo;
    private static int limit = 25000000;
    private static boolean flag = true;
    private static String answer = "y";
    public static void SetorTunai() {

        while (flag) {
            System.out.print("Jumlah Uang yang ingin diSetor : ");
            jumlah = input.nextLine();

            if (Utility.IsNumber(jumlah)) {
                int tunai = Integer.parseInt(jumlah);
                if (tunai <= limit) {
                    System.out.println("Apakah Anda yakin? y/n");
                    answer = input.nextLine();
                    System.out.println("Setoran Anda " + jumlah);
                    System.out.println("transaksi Setoran Anda berhasil");
                    System.out.println("Apakah Anda Ingin Transaksi Kembali? (y/n)");
                    String jawaban = input.nextLine();
                    if (jawaban.toLowerCase().equals("y")) {
                        flag = false;
                        Pin.BuatPin();
                    } else {
                        flag = false;
                        break;
                    }
                } else {
                    System.out.println("Setoran Anda terkena Limit");
                }
            }
        }
    }
}
