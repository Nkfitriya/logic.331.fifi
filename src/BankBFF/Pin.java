package BankBFF;


import java.util.Scanner;

public class Pin {
    private static String buatpin;
    private static String cekpin;
    private static Scanner input = new Scanner(System.in);
    public static void BuatPin(){
        boolean flag = true;

        while (flag) {
            System.out.println("Silahkan Buat PIN anda (6 digit, Harus berupa Angka)");
            buatpin = input.nextLine();

            if(buatpin.length() != 6) {
                System.out.println("Pin Harus 6 Digit");
            }else {
                if (!Utility.IsNumber(buatpin)) {
                    System.out.println("Pin Harus Berupa Angka");
                } else {
                    System.out.println("Pin Berhasil dibuat");
                    flag = false;
                    break;
                }
            }
        }
    }

}